
import CategoryLists from "./categorylists"
import ProductBestLists from "./productbestlists"
import TipsNTricks from "./tipsntricks"
import Payments from "./payments"
import CarouselSlider from "./carousel"
import Layout from "../components/layouts/main"

export default function Home() {
  return (
    <Layout>
      <CarouselSlider />
      <CategoryLists title="Product Categories" />
      <ProductBestLists title="Best Seller Products" />
      {/* <FavoriteProducts title="Favorit Products" /> */}
      <TipsNTricks title="Tips and Tricks" />
      <Payments />
    </Layout>
  )
}
