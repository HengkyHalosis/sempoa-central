import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Styles from './componentStyles.module.css'
import { FaHeart, FaRegHeart } from 'react-icons/fa';

const productBestLists = (props) => {
    const categories = [1,2,3,4]
    return (
        <section className="product-lists-section py-3">
            <Container>
                <h2>{props.title}</h2>
                <Row className="product-lists-wrapper">
                    {categories.map((category, idx) => (
                        <Col xs={6} sm={4} md={3} key={idx} className="product-item mb-4 px-2">
                            <div className={`${Styles.productWrapper}`}>
                                <div className={`${Styles.productImgWrapper}`}>
                                    <div className={`${Styles.productImgContent}`}>
                                        <img src="/images/placeholder-image.png" className={`${Styles.imgProduct}`} alt="placeholder"/>
                                    </div>
                                </div>
                                <div className={`${Styles.productDetails} bg-gray`}>
                                    <Container>
                                        <h5 className="color-primary font-weight-bold mb-0">Product Name</h5>
                                        <div className="product-description text-secondary">Product Descriptions</div>
                                        <div className="product-price color-primary font-weight-bold mb-2">
                                            Rp. 389.000
                                        </div>
                                        <div className={`${Styles.addToCart} m-0`}>
                                            <div className="px-2 bg-gray border-color-primary mr-1 rounded">
                                                <FaRegHeart className="color-primary" />
                                            </div>
                                            <div className="btn-addtocart button-hover px-2 bg-color-primary flex-grow-1 text-center text-white rounded">Add to Cart</div>
                                        </div>
                                    </Container>
                                </div>
                            </div>
                        </Col>
                    ))}
                </Row>
                <Container>
                    <Row>
                        <Col xs={6} sm={4} md={3} className={`${Styles.seeMore} color-primary border-color-primary`}>See more</Col>
                    </Row>
                </Container>
            </Container>

            <style jsx>
                {`
                    @media(max-width: 991px) {
                        .btn-addtocart {
                            font-size: .7rem;
                            line-height: 1.6rem;
                        }
                    }
                    @media(max-width: 575px) {
                        .btn-addtocart {
                            font-size: 1rem;
                            line-height: 1.6rem;
                        }
                    }
                    @media(max-width: 388px) {
                        .btn-addtocart {
                            font-size: 3vw;
                            line-height: 1.6rem;
                        }
                    }
                `}
            </style>
        </section>
    );
};

export default productBestLists;