import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import footerStyles from './footer.module.css'
import { FaFacebookF, FaInstagram, FaTwitter, FaYoutube } from 'react-icons/fa';

const Footer = () => {
    return (
        <section className="footer-section py-3 bg-gray">
            <Container>
                <div className="text-center text-secondary">
                    &copy;{new Date().getFullYear()} | Central Supermarket
                </div>
            </Container>
        </section>
    );
};

export default Footer;