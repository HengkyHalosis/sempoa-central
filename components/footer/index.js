import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import footerStyles from './footer.module.css'
import { FaFacebookF, FaInstagram, FaTwitter, FaYoutube } from 'react-icons/fa';

const Footer = () => {
    return (
        <section className="footer-section pt-5 pb-3 bg-gray">
            <Container>
                <Row className="small">
                    <Col sm={6} lg={3} className="footer-widget first text-secondary mb-3 mb-lg-0">
                        <h5 className="footer-widget-title">Central Supermarket</h5>
                        <strong>Address</strong>
                        <p>
                            Store & Office<br/>
                            Jl. Letjend Suprapto No. 3,<br/>
                            Margasari, Balikpapan Barat, Kalimantan Timur 76131<br/>
                        </p>
                        <strong>Office Hour</strong>
                        <p>
                            Monday - Sunday<br/>
                            10.00 - 18.00<br/>
                        </p>
                    </Col>
                    <Col sm={6} lg={3} className="footer-widget second text-secondary">
                        <h5 className="footer-widget-title">Get in touch</h5>
                        <Row className="mb-1">
                            <Col xs={5}>
                                <strong>Address</strong>
                            </Col>
                            <Col xs={7}>
                                <span>0542 421533</span>
                            </Col>
                        </Row>
                        <Row className="mb-1">
                            <Col xs={5}>
                                <strong>Email</strong>
                            </Col>
                            <Col xs={7}>
                                <span>admin@central.com</span>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={5}>
                                <strong>Location</strong>
                            </Col>
                            <Col xs={7}>
                                <span>Google Maps</span>
                            </Col>
                        </Row>
                        <div className="d-flex mt-3">
                            <div className={`${footerStyles.socialIconsPrimary} bg-color-primary mr-2`}>
                                <FaFacebookF />
                            </div>
                            <div className={`${footerStyles.socialIconsPrimary} bg-color-primary mr-2`}>
                                <FaInstagram />
                            </div>
                            <div className={`${footerStyles.socialIconsPrimary} bg-color-primary mr-2`}>
                                <FaTwitter />
                            </div>
                            <div className={`${footerStyles.socialIconsPrimary} bg-color-primary`}>
                                <FaYoutube />
                            </div>
                        </div>
                    </Col>
                    <Col sm={6} lg={3} className="footer-widget third text-secondary">
                        <h5 className="footer-widget-title">Information</h5>
                        <ul className="list-unstyled">
                            <li>FAQ</li>
                            <li>Terms & Conditions</li>
                            <li>Privacy Policy</li>
                            <li>How To Buy</li>
                            <li>Sitemap</li>
                        </ul>
                    </Col>
                    <Col sm={6} lg={3} className="footer-widget fourth text-secondary">
                        <h5 className="footer-widget-title">My Account</h5>
                        <ul className="list-unstyled">
                            <li>My Account</li>
                            <li>Wishlist</li>
                            <li>Inquiry List</li>
                            <li>Shopping Cart</li>
                            <li>My Orders</li>
                            <li>Track Shipment</li>
                        </ul>
                    </Col>
                    {/* <Col lg={9} className="footer-widget payment-logo-wrapper offset-lg-3">
                        <div className={`${footerStyles.paymentLogos} d-flex flex-wrap align-items-center`}>
                            <img src="/images/gopay.png" alt="gopay" height={13} className={`${footerStyles.paymentImage} px-2`} />
                            <img src="/images/visa-master-card.png" alt="visa master card" height={13} className={`${footerStyles.paymentImage} px-2`} />
                            <img src="/images/bca.png" alt="visa master card" height={13} className={`${footerStyles.paymentImage} px-2`} />
                            <img src="/images/bni.png" alt="visa master card" height={13} className={`${footerStyles.paymentImage} px-2`} />
                            <img src="/images/bri.png" alt="visa master card" height={13} className={`${footerStyles.paymentImage} px-2`} />
                            <img src="/images/mandiri.png" alt="visa master card" height={13} className={`${footerStyles.paymentImage} px-2`} />
                            <img src="/images/permata.png" alt="visa master card" height={13} className={`${footerStyles.paymentImage} px-2`} />
                            <img src="/images/atm-bersama.png" alt="visa master card" height={13} className={`${footerStyles.paymentImage} px-2`} />
                            <img src="/images/alfamart.png" alt="visa master card" height={13} className={`${footerStyles.paymentImage} px-2`} />
                        </div>
                    </Col> */}
                </Row>
                <div className="text-center text-secondary">
                    &copy;{new Date().getFullYear()} | Central Supermarket
                </div>
            </Container>
        </section>
    );
};

export default Footer;