// Helper styles for demo
import React from "react";

import Select from "react-select";


class Multiselect extends React.Component {
  handleChange = value => {
    // this is going to call setFieldValue and manually update values.topcis
    this.props.onChange(this.props.name, value);
  };

  handleBlur = () => {
    // this is going to call setFieldTouched and manually update touched.topcis
    this.props.onBlur(this.props.name, true);
  };

  render() {
    return (
      <>
        <Select
          id={this.props.instanceId}
          instanceId={this.props.instanceId}
          options={this.props.options}
          isMulti
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.props.value}
        />
        {!!this.props.error && this.props.touched && (
            <div className="error small text-danger text-right">{this.props.error}</div>
        )}
      </>
    );
  }
}

export default Multiselect


// USAGE EXAMPLE:
// <Multiselect
//     name="products"
//     instanceId="product-central"
//     options={products}
//     value={values.products}
//     onChange={setFieldValue}
//     onBlur={setFieldTouched}
//     error={errors.products}
//     touched={touched.products}
//     dirty={dirty.products}
// />