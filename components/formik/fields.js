import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { Formik, Form, useField, FieldProps, useFormikContext, ErrorMessage } from "formik";
import * as Yup from "yup";
import styled from "@emotion/styled";
import { FaEye, FaEyeSlash } from 'react-icons/fa'

export const InputText = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={props.id || props.name} className="color-primary form-label">{label}</label>
      <input className="text-input form-control" {...field} {...props} />
      {meta.touched && meta.error ? (
        <div className="error small text-danger text-right">{meta.error}</div>
      ) : null}
    </>

    
    // USAGE EXAMPLE: 
    // <InputText label="Email" name="email" type="email" placeholder="your.email@mail.com" />
  );
};

export const InputTextArea = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={props.id || props.name} className="color-primary form-label">{label}</label>
      <textarea className="form-control" {...field} {...props}></textarea>
      {meta.touched && meta.error ? (
      <div className="error small text-danger text-right">{meta.error}</div>
      ) : null}
    </>
    
    // USAGE EXAMPLE: 
    // <InputTextArea name="textarea1" rows={3} id="textarea1" for="textarea1" label="text area" />
  );
};

export const InputPassword = ({ label, seePassword, setSeePassword, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={props.id || props.name} className="color-primary form-label">{label}</label>
      <div className="input-group">
        <input className="text-input form-control" {...field} {...props} />
        <div className="input-group-append">
            <div className="input-group-text">
                
            {seePassword ? <FaEyeSlash onClick={() => setSeePassword(false)} /> : <FaEye onClick={() => setSeePassword(true)} />}
            </div>
        </div>
      </div>
      <ErrorMessage name={props.name} />
      {meta.touched && meta.error ? (
        <div className="error small text-danger text-right">{meta.error}</div>
      ) : null}
    </>

    
    // USAGE EXAMPLE:
    // <InputPassword label="Password" name="password" type={seePassword ? "text" : "password"}  placeholder="" seePassword={seePassword} setSeePassword={setSeePassword} />
  );
};

export const InputCheckbox = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
    <div className="small color-primary form-check">
      <input className="form-check-input" id={props.id} {...field} {...props} />
      <label title={props.title} htmlFor={props.for} className="form-check-label">{label}</label>
    </div>
      {meta.touched && meta.error ? (
        <div className="error small text-danger text-right">{meta.error}</div>
      ) : null}
    </>

    
    // USAGE EXAMPLE:
    // <InputCheckbox name="rememberMe" label="Remember Me" type="checkbox" id="rememberMeCheckBox" for="rememberMeCheckBox" />
  );
};

export const InputSelect = ({ children, label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <label className="color-primary form-label">{label}</label>
      <select className="form-control" {...field} {...props}>
        {children}
      </select>
      {meta.touched && meta.error ? (
        <div className="error small text-danger text-right">{meta.error}</div>
        ) : null}

      
      {/* USAGE EXAMPLE:
      <InputSelect name="select1" label="select 1">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
      </InputSelect> */}
    </>
  );
};


