import { useState, useRef, useEffect } from 'react'
import router, {useRouter} from 'next/router'
import styles from './style-header.module.css'
import { 
  Container, 
  Row,
  Col
} from 'react-bootstrap'
import { FaFacebookF, FaTwitter, FaInstagram, FaShieldAlt } from 'react-icons/fa'
import { MdLocalShipping } from 'react-icons/md'
import { IoMdMedal, IoIosMedal } from 'react-icons/io'
import Link from 'next/link'

function Top() {
    const route = useRouter();
    const [menuAbout, setMenuAbout] = useState(false)

    const nodeAbout = useRef();

    useEffect(() => {
        // add when mounted
        document.addEventListener("mousedown", handleClickMenuAbout);
        // return function to be called when unmounted
        return () => {
            document.removeEventListener("mousedown", handleClickMenuAbout);
        };
    }, []);

    const handleClickMenuAbout = e => {
        if (nodeAbout.current.contains(e.target)) {
            // inside click
            return;
        }else {
            // outside click 
            setMenuAbout(false)
        }
        
    };

    
    const AboutDropdown = () => {
        return (
            <div className={`${styles.menuAbout} small`}>
                <ul className="list-unstyled m-0">
                    {["Company Overview", "Organization Structure", "Vision & Mision"].map((item, index) => 
                    <Link href={`/product_category/${item.name}`}>
                        <li key={index} className={`${styles.menuAboutItem} color-primary`} onClick={() => {}}>
                            {item}
                        </li>
                    </Link>
                    )}
                </ul>
            </div>
        )
    }

    return (
        <section className='bg-color-primary text-white py-3'>
            <Container>
                <Row className="justify-content-between align-items-center mx-0">
                    <div ref={nodeAbout} className="about mb-2 mb-md-0 mx-auto mx-sm-0 pointer position-relative">
                        <h4 className="m-0" onClick={() => setMenuAbout(!menuAbout)}>About Us</h4>
                        {menuAbout ? <AboutDropdown /> : null}
                    </div>
                    <div className="features mb-3 mb-md-0">
                        <Row className="features-row mx-0 justify-content-center">
                            <div className="feature-item px-2">
                                <IoIosMedal /> Warranty Protection
                            </div>
                            <div className="feature-item px-2">
                                <FaShieldAlt /> High Quality
                            </div>
                            <div className="feature-item px-2">
                                <MdLocalShipping /> Free Shipping*
                            </div>
                        </Row>
                    </div>
                    <div className="social-icons-wrapper ml-auto ml-md-0">
                        <Row className="mx-0">
                            <div className={`${styles.socialIcons} mr-2`}>
                                <FaFacebookF />
                            </div>
                            <div className={`${styles.socialIcons} mr-2`}>
                                <FaInstagram />
                            </div>
                            <div className={styles.socialIcons}>
                                <FaTwitter />
                            </div>
                        </Row>
                    </div>
                </Row>
            </Container>
        </section>
    );
}

export default Top;