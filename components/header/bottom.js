import styles from './style-header.module.css'
import { 
  Container, 
  Row,
  Col
} from 'react-bootstrap'
import { FaFacebookF, FaTwitter, FaInstagram, FaShieldAlt, FaUserShield, FaMedal, FaSearch, FaHeart, FaUser } from 'react-icons/fa'
import { MdLocalShipping, MdLoop, MdSearch, MdShoppingCart } from 'react-icons/md'
import { IoMdMedal, IoIosMedal } from 'react-icons/io'
import Link from 'next/link'

import { useRecoilValue, useSetRecoilState, useResetRecoilState } from 'recoil'
import { isLogin, setIsLoginTrue, setIsLoginFalse } from '../../lib/recoil-atoms'

const loginStatus = () => ({
  isLogin: useRecoilValue(isLogin),
  login: useSetRecoilState(setIsLoginTrue),
  logout: useSetRecoilState(setIsLoginFalse),
})

function Bottom() {
    const { isLogin, login, logout } = loginStatus()
    return (
        <section className='brand-section py-4'>
            <Container>
                {/* {`status: ${isLogin ? "anda signed in" : "silakan sign in"}`} <span onClick={()=> isLogin ? logout : login }>{isLogin ? "logout" : "login"}</span> */}
                <Row className="mx-0 align-items-center">
                    <div className="brand-logo mr-5 mb-2 mb-lg-0">
                        <Link href="/">
                            <a className="text-decoration-none color-primary">
                                <img src="/images/central-logo.svg" alt="Central Shopping"/>
                            </a>
                        </Link>
                    </div>
                    <div className={`${styles.searchBarWrapper} search-bar-wrapper bg-gray rounded px-5 py-2 mb-3 mb-md-0 position-relative`}>
                        <MdSearch className={`${styles.searchIcon}`} />
                        <input type="text" name="search" id="search" className={`${styles.searchBar}`} placeholder="Search products" />
                    </div>

                    <div className="notif-bar ml-auto">
                        <Row className="mx-0">
                            <div className="notif-wishlist mr-3 d-flex align-items-center">
                                <div className={`${styles.notifIcon} notif-icon d-flex align-items-center position-relative`}>
                                    <FaHeart className="color-primary mr-2" />
                                    <div className={`${styles.notifWhislist}`}>18</div>
                                </div>
                                Whishlist
                            </div>
                            <div className="notif-wishlist mr-3 d-flex align-items-center">
                                <div className={`${styles.notifIcon} notif-icon d-flex align-items-center position-relative`}>
                                    <div className={`${styles.notifWhislist}`}>20</div>
                                    <MdShoppingCart className="color-primary mr-2" />
                                </div>
                                My Cart
                            </div>
                            <div className="notif-wishlist d-flex align-items-center">
                                <div className="notif-icon d-flex align-items-center">
                                    <FaUser className="color-primary mr-2" />
                                </div>
                                My Profile
                            </div>
                        </Row>
                    </div>
                </Row>
            </Container>
            <style jsx>
                {`
                    @media(max-width: 991px) {
                        .brand-logo {
                            width: 100%
                        }
                    }
                    @media(max-width: 575px) {
                        .search-bar-wrapper {
                            width: 100%
                        }
                        .brand-logo {
                            text-align: center;
                        }
                    }
                `}
            </style>
        </section>
    );
}

export default Bottom;