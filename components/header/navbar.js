import { useState, useRef, useEffect } from 'react'
import styles from './style-header.module.css'
import {
    Container,
    Row,
    Col
} from 'react-bootstrap'
import { FaThLarge, FaChevronRight } from 'react-icons/fa'
import { } from 'react-icons/md'
import { } from 'react-icons/io'
import router, {useRouter} from 'next/router'
import Link from 'next/link'

const Categories = [
    {
        name: 'Category_One',
        level_two: [
            { name2: "Cat_One_Level_two_1" },
            { name2: "Cat_One_Level_two_2" },
        ]
    },
    {
        name: 'Category_Two',
        level_two: [
            { name2: "Cat_Two_Level_two_1" },
            { name2: "Cat_Two_Level_two_2" },
        ]
    },
    {
        name: 'Category_Three',
        level_two: [
            { name2: "Cat_Three_Level_two_1" },
            { name2: "Cat_Three_Level_two_2" },
        ]
    },
    {
        name: 'Category_Four',
        level_two: [
            { name2: "Cat_Four_Level_two_1" },
            { name2: "Cat_Four_Level_two_2" },
        ]
    },
    {
        name: 'Category_Five',
        level_two: [
            { name2: "Cat_Five_Level_two_1" },
            { name2: "Cat_Five_Level_two_2" },
        ]
    },
    {
        name: 'Category_Six',
        level_two: [

        ]
    }
]


function Top() {
    const route = useRouter();
    console.log(route)

    const [categoryLevel1, setCategoryLevel1] = useState(false)
    const [categoryLevel2, setCategoryLevel2] = useState("")
    const [brands, setBrands] = useState(false)
    const [promotions, setPromotions] = useState(false)

    const node1 = useRef();
    const node2 = useRef();
    const node3 = useRef();

    useEffect(() => {
        // add when mounted
        document.addEventListener("mousedown", handleClickCategories);
        document.addEventListener("mousedown", handleClickProductBrand);
        document.addEventListener("mousedown", handleClickPromotions);
        // return function to be called when unmounted
        return () => {
            document.removeEventListener("mousedown", handleClickCategories);
            document.removeEventListener("mousedown", handleClickProductBrand);
            document.removeEventListener("mousedown", handleClickPromotions);
        };
    }, []);

    const handleClickCategories = e => {
        if (node1.current.contains(e.target)) {
            // inside click
            return;
        }else {
            // outside click 
            setCategoryLevel1(false)
            setCategoryLevel2("")
        }
        
    };
    const handleClickProductBrand = e => {
        if (node2.current.contains(e.target)) {
            // inside click
            return;
        }else {
            // outside click 
            setBrands(false)
        }
        
    };
    const handleClickPromotions = e => {
        if (node3.current.contains(e.target)) {
            // inside click
            return;
        }else {
            // outside click 
            setPromotions(false)
        }
        
    };

    const CategoryLevelOne = () => {
        return (
            <div className={`${styles.categoryLevelOne}`}>
                <ul>
                    {Categories.map((cat, index) => (
                        <Link href={`/product_category/${cat.name}`}>
                            <li key={cat.name} className="pointer position-relative color-primary font-weight-bold" onMouseEnter={() => setCategoryLevel2(cat.name)} onMouseLeave={() => setCategoryLevel2("")}>
                                {cat.name}
                                {cat.level_two.length !== 0 ? <FaChevronRight className={`${styles.chevronRightStyle}`} /> : null}
                                {categoryLevel2 == cat.name ? <CategoryLevelTwo cat={cat} /> : null}
                            </li>
                        </Link>
                    ))}
                </ul>
            </div>
        )
    }

    const CategoryLevelTwo = ({ cat }) => {
        return (
            <div className={`${styles.categoryLevelTwo}`}>
                <ul>
                    {cat.level_two.map((catLevel2, index) => (
                        <Link as={`/product_category/${cat.name}/${catLevel2.name2}`} href={`/product_category/${route.query.category_one}/${route.query.category_two}`}>
                            <li key={catLevel2.name2} className="pointer color-primary font-weight-bold" style={{ width: "max-content" }}>
                                {catLevel2.name2}
                            </li>
                        </Link>
                    ))}
                </ul>
            </div>
        )
    }

    const ProductBrands = () => {
        return (
            <Container className={`${styles.productBrands}`}>
                <Row>
                    {[1,2,3,4,5,6,7,8,9,10,11,12].map((item, index) =>
                        <Col key={item} xs={6} sm={4} md={3} lg={2}>
                            <div className={`${styles.brandImgWrapper}`}>
                                <div className={`${styles.brandImgContent}`}>
                                    <img src="/images/placeholder-image.png" className={`${styles.imgBrand}`} alt="placeholder" />
                                </div>
                            </div>
                        </Col>
                    )}
                </Row>
                <div className="see-more text-center color-primary col-sm-3 mx-auto py-1 primary-button">See More</div>
            </Container>
        )
    }

    const Promotions = () => {
        return (
            <Container className={`${styles.productBrands}`}>
                <Row>
                    {[1,2].map((item, index) =>
                        <Col key={item} sm={6}>
                            <div className={`${styles.promotionWrapper} color-primary my-3`}>
                                <div className={`${styles.promotionPeaceTop}`}>
                                    <img src="/images/ticketpromotiontop.png" alt="central promotion ticket" className={`${styles.tiketPromotionTop}`}/>
                                    <div className="py-5 px-5 px-sm-3 px-md-5">
                                        <h5 className="font-weight-bold">Gopay Diskon Langsung 15K</h5>
                                        {`Pakai Gopay diskon 15K dengan Min. transaksi sebesar 200K`}
                                    </div>
                                </div>
                                <div className={`${styles.promotionPeaceBottom} border-scoop`}>
                                    <img src="/images/ticketpromotionbottom.png" alt="central promotion ticket" className={`${styles.tiketPromotionBottom}`}/>
                                    <div className="py-4 px-5 px-sm-3 px-md-5 d-flex justify-content-between ">
                                        <div className="promotion-code font-weight-bold">
                                            {`GPYBLM`}
                                        </div>
                                        <div className="copy-code-btn">
                                            Salin
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    )}
                </Row>
            </Container>
        )
    }


    // const showLevelTwo = (e, cat) => {
    //     e.stopPropagation()
    //     setCategoryLevel2(cat)
    // }

    return (
        <section className='bg-color-primary text-white py-4'>
            <Container>
                <Row className="justify-content-between align-items-center mx-0">
                    <div ref={node1} className="nav-bar-item mb-2 mb-md-0 position-relative" onClick={() => setCategoryLevel1(!categoryLevel1)}>
                        <h4 className="pointer"><FaThLarge size={28} className="mr-3" />Product Category</h4>
                        {categoryLevel1 ? <CategoryLevelOne /> : null}
                    </div>
                    <div ref={node2} className="nav-bar-item mb-2 mb-md-0" onClick={() => setBrands(!brands)}>
                        <h4 className="pointer">Product Brands</h4>
                        {brands ? <ProductBrands /> : null}
                    </div>
                    <div className="nav-bar-item mb-2 mb-md-0">
                        <Link href="/inquiry">
                            <h4 className="pointer">Product Inquiry</h4>
                        </Link>
                    </div>
                    <div ref={node3} className="nav-bar-item mb-2 mb-md-0" onClick={() => setPromotions(!promotions)}>
                        <h4 className="pointer">Promotions</h4>
                        {promotions ? <Promotions /> : null}
                    </div>
                </Row>
            </Container>
            <style>{`
                @media(max-width: 575px) {
                    .nav-bar-item h4 {
                        font-size: 85%;
                    }
                }
                @media(max-width: 767px) {
                    .nav-bar-item h4 {
                        font-size: 90%;
                    }
                }
            `}</style>
        </section>
    );
}

export default Top;