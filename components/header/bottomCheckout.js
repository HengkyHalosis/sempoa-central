import styles from './style-header.module.css'
import { 
  Container, 
  Row,
  Col
} from 'react-bootstrap'
import {  } from 'react-icons/fa'
import {  } from 'react-icons/md'
import Link from 'next/link'

function BottomCheckout({ confirm }) {
    return (
        <section className='brand-section py-4'>
            <Container>
                <Row className="mx-0 align-items-center">
                    <Col md={2} className="brand-logo mb-2 mb-lg-0">
                        <Link href="/">
                            <a className="text-decoration-none color-primary">
                                <img src="/images/central-logo.svg" alt="Central Shopping"/>
                            </a>
                        </Link>
                    </Col>
                    <Col md={10}>
                        <div className={`${styles.paymentSteps} px-3`}>
                            <div className={`${styles.stepOneIcon} ${styles.stepIcon}`}>
                                <img src="/images/checkout-icon.svg" alt="central checkout icon"/>
                                <div className="small color-primary">Checkout</div>
                            </div>
                            <div className={`${confirm == "delivered" ? styles.spacerLineColor : styles.spacerLine}`}></div>
                            <div className={`${styles.stepTwoIcon}  ${styles.stepIcon}`}>
                                {confirm == "delivered" ? 
                                <img src="/images/payment-icon-color.svg" alt="central payment icon"/>
                                : 
                                <img src="/images/payment-icon.svg" alt="central payment icon"/>
                                }
                                <div className={confirm == "delivered" ? "color-primary small" : "small"}>Payment</div>
                            </div>
                            <div className={`${confirm == "delivered" ? styles.spacerLineColor : styles.spacerLine}`}></div>
                            <div className={`${styles.stepThreeIcon}  ${styles.stepIcon}`}>
                                {confirm == "delivered" ? 
                                <img src="/images/confirm-color.svg" alt="central payment icon"/>
                                : 
                                <img src="/images/confirm.svg" alt="central payment icon"/>
                                }
                                <div className={confirm == "delivered" ? "color-primary small" : "small"}>Confirmation</div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
            <style jsx>
                {`
                    @media(max-width: 991px) {
                        .brand-logo {
                            width: 100%
                        }
                    }
                    @media(max-width: 575px) {
                        .search-bar-wrapper {
                            width: 100%
                        }
                        .brand-logo {
                            text-align: center;
                        }
                    }
                `}
            </style>
        </section>
    );
}

export default BottomCheckout;