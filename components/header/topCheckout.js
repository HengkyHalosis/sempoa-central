import router, {useRouter} from 'next/router'
import styles from './style-header.module.css'
import { 
  Container, 
  Row,
  Col
} from 'react-bootstrap'
import { FaShieldAlt } from 'react-icons/fa'
import { MdLocalShipping } from 'react-icons/md'
import { IoIosMedal } from 'react-icons/io'

function TopCheckout() {
    const route = useRouter();

    return (
        <section className='bg-color-primary text-white py-3'>
            <Container>
                <Row className="justify-content-center align-items-center mx-0">
                    <div className="features mb-3 mb-md-0">
                        <Row className="features-row mx-0 justify-content-center">
                            <div className="feature-item px-2">
                                <IoIosMedal /> Warranty Protection
                            </div>
                            <div className="feature-item px-2">
                                <FaShieldAlt /> High Quality
                            </div>
                            <div className="feature-item px-2">
                                <MdLocalShipping /> Free Shipping*
                            </div>
                        </Row>
                    </div>
                </Row>
            </Container>
        </section>
    );
}

export default TopCheckout;