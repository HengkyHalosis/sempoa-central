import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Styles from './componentStyles.module.css'
import { FaHeart, FaRegHeart } from 'react-icons/fa';

const Tipsntricks = () => {
    return (
        <section className="py-5 bg-gray">
            <Container>
                <Row>
                    {[1,2,3].map((item, idx) => (
                    <Col sm={4} key={idx} className="tip-trick-item mb-4 px-2 position-relative">
                        <div className={`${Styles.tipstricksWrapper} shadow bg-white`}>
                            <div className={`${Styles.tipstricksImgWrapper}`}>
                                <div className={`${Styles.tipstricksImgContent}`}>
                                    <img src="/images/placeholder-image.png" className={`${Styles.imgProduct}`} alt="placeholder" />
                                </div>
                                <div className={`${Styles.tipstricksDetails} bg-gray`}>
                                    <Container>
                                        <p className="mb-0 small text-secondary">Tips & Tricks descriptions</p>
                                        <strong className="product-description text-secondary">Tips & Tricks Title</strong>
                                    </Container>
                                </div>
                            </div>
                        </div>
                    </Col>
                    ))}
                </Row>
            </Container>
        </section>
    );
};

export default Tipsntricks;