import React from 'react';
import { Carousel, Container } from 'react-bootstrap'
import Styles from './componentStyles.module.css'

const carouselSlider = () => {
    return (
        <section id="carousel-section">
            <Container>
                <Carousel interval={3000}>
                    <Carousel.Item>
                        <div className={`${Styles.imgCarouselWrapper}`}>
                            <img src="/images/placeholder-image.png" className={`${Styles.imgCarousel}`} alt="placeholder"/>
                        </div>
                        <Carousel.Caption>
                            <h3>First slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <div className={`${Styles.imgCarouselWrapper}`}>
                            <img src="/images/placeholder-image.png" className={`${Styles.imgCarousel}`} alt="placeholder"/>
                        </div>
                        <Carousel.Caption>
                            <h3>Second slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <div className={`${Styles.imgCarouselWrapper}`}>
                            <img src="/images/placeholder-image.png" className={`${Styles.imgCarousel}`} alt="placeholder"/>
                        </div>
                        <Carousel.Caption>
                            <h3>Third slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </Container>
        </section>
    );
};

export default carouselSlider;