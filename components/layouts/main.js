import Head from 'next/head'
import HeaderTop from "../header/top"
import HeaderBottom from "../header/bottom"
import Navbar from "../header/navbar"
import Footer from "../footer"

export default ({ children, title = 'Central' }) => (
  <>
    <Head>
      <title>{ title }</title>
        <link rel="icon" href="/favicon.ico" />
    </Head>
    <header>
        <HeaderTop />
        <HeaderBottom />
        <Navbar />
    </header>

    <main>
      { children }
    </main>

    <footer>
        <Footer />
    </footer>
  </>
)