import Head from 'next/head'
import HeaderTop from "../header/topCheckout"
import HeaderBottomCheckout from "../header/bottomCheckout"
import FooterCheckout from "../footer/footerCheckout"

export default ({ children, title = 'Central', confirm }) => (
  <div style={{ minHeight: "100vh", paddingBottom: 90, position: "relative"}}>
    <Head>
      <title>{ title }</title>
        <link rel="icon" href="/favicon.ico" />
    </Head>
    <header style={{ boxShadow: "0 0 10px #ccc" }}>
        <HeaderTop />
        <HeaderBottomCheckout confirm={confirm} />
    </header>

    <main>
      { children }
    </main>

    <footer style={{ position: "absolute", left: 0, right: 0, bottom: 0 }}>
        <FooterCheckout />
    </footer>
  </div>
)