import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Styles from './componentStyles.module.css'
import { FaArrowRight } from 'react-icons/fa';

const FavoriteProducts = () => {
    return (
        <section className="bg-gray py-5">
            <Container>
                <Row className="d-flex">
                    <Col md={3} className=" mb-3 mt-3 mt-md-0 d-flex flex-column justify-content-center">
                        <h2 className="color-primary font-weight-bold">Our Favorite Products</h2>
                        <p>for more information please click button below.</p>
                        <div className="fav-button button-hover bg-color-primary text-white text-center rounded">Explore More</div>
                    </Col>
                    <Col md={9} className="">
                        <Row>
                            <Col sm={6} className="px-2 mb-3 mb-sm-0">
                                <div className={`${Styles.favProductWrapper} bg-color-primary-light`}>
                                    <div className={`${Styles.favProductImgWrapper} rounded`}>
                                        <div className={`${Styles.favProductImgContent} rounded`}>
                                            <img src="/images/placeholder-image.png" className={`${Styles.imgProduct}`} alt="placeholder"/>
                                        </div>
                                        <div className={`${Styles.favProductCard}`}>
                                            <div className="fav-product-desc">
                                                Lorem ipsum dolor sit amet.
                                            </div>
                                            <h4>Vegetables</h4>
                                            <div className={`${Styles.arrowButton} bg-color-primary`}>
                                                <FaArrowRight />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col sm={6} className="px-2 mb-3 mb-sm-0">
                                <div className={`${Styles.favProductWrapper} bg-color-primary-light`}>
                                    <div className={`${Styles.favProductImgWrapper}`}>
                                        <div className={`${Styles.favProductImgContent}`}>
                                            <img src="/images/placeholder-image.png" className={`${Styles.imgProduct}`} alt="placeholder"/>
                                        </div>
                                        <div className={`${Styles.favProductCard}`}>
                                            <div className="fav-product-desc">
                                                Lorem ipsum dolor sit amet.
                                            </div>
                                            <h4>Snack Time</h4>
                                            <div className={`${Styles.arrowButton} bg-color-primary`}>
                                                <FaArrowRight />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </section>
    );
};

export default FavoriteProducts;