import React from 'react';
import Image from 'next/image'
import { Container, Row } from 'react-bootstrap';
import Styles from './componentStyles.module.css'

const Payments = () => {
    const array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
    return (
        <section className="bg-color-primary py-5">
            <Container>
                <div className="d-flex flex-wrap justify-content-center">
                    {array.map((item, index) => (
                        <div key={index} className="py-4 px-3 px-md-4 px-lg-5">
                            <img src="/images/no-image.png" className={`${Styles.imgPayments}`} alt="placeholder" />
                        </div>  
                    )
                    )}
                </div>
            </Container>
        </section>
    );
};

export default Payments;