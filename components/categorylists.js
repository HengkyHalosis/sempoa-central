import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Styles from './componentStyles.module.css'

const categorylists = (props) => {
    const categories = [1,2,3,4,5,6,7,8]
    return (
        <section className="product-lists-section py-3">
            <Container>
                <h2>{props.title}</h2>
                <Row className="product-lists-wrapper">
                    {categories.map((category, idx) => (
                        <Col xs={6} sm={4} md={3} key={idx} className="product-item mb-4 px-2">
                            <div className={`${Styles.productWrapper}`}>
                                <div className={`${Styles.productImgWrapper}`}>
                                    <div className={`${Styles.productImgContent}`}>
                                        <img src="/images/placeholder-image.png" className={`${Styles.imgProduct}`} alt="placeholder"/>
                                    </div>
                                </div>
                                <div className={`${Styles.categoryTitle} color-primary bg-gray`}>Product Category #{category}</div>
                            </div>
                        </Col>
                    ))}
                </Row>
            </Container>
        </section>
    );
};

export default categorylists;