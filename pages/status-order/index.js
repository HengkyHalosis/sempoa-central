import React, { useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { FaArrowLeft, FaLongArrowAltLeft } from 'react-icons/fa';
import Layout from '../../components/layouts/main'
import styles from './styles.module.css'
import Link from 'next/link'

const Checkout = () => {

    return (
        <Layout title="Status Order">
            <section className="py-4">
                <Container>
                    <div className="back mb-3">
                        <Link href="/transaction"><a className="text-decoration-none color-primary"><FaLongArrowAltLeft /> Kembali ke Transaksi</a></Link>
                    </div>  
                                      
                    <Row>
                        <Col md={8}>
                            <div className="d-flex flex-column">
                                <div className="status-pesanan border-color-secondary p-3">
                                    <Container>
                                        <Row className="small pb-2 border-bottom">
                                            <Col xs={5}>
                                                <div className="orderNumber color-primary">
                                                    <h6 className="color-primary">
                                                        No Pesanan : 8381157
                                                    </h6>
                                                </div>
                                            </Col>
                                            <Col xs={7}>
                                                <div className="orderTime color-primary text-md-right">
                                                    Tanggal Pemesanan : 24 November 2020, 11:15 WIT
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row className="small pt-2 pb-5 border-bottom">
                                            <Container>
                                                <h6 className="color-primary">
                                                    Status Pesanan
                                                </h6>
                                                <div className="d-flex align-items-center py-4 py-sm-5 justify-content-center">
                                                    <div className={`${styles.statusIcon} position-relative`}>
                                                        <img src="/images/sand-timer-color.svg" alt="sand timer"/>
                                                        <div className={`${styles.statusText}`}>Menunggu<br />Pembayaran</div>
                                                    </div>
                                                    <div className={`${styles.liner}`}></div>
                                                    <div className={`${styles.statusIcon} position-relative`}>
                                                        <img src="/images/thumbup-color.svg" alt="thumb up"/>
                                                        <div className={`${styles.statusText}`}>Pembayaran<br />Diterima</div>
                                                    </div>
                                                    <div className={`${styles.linerGray}`}></div>
                                                    <div className={`${styles.statusIconGray} position-relative`}>
                                                        <img src="/images/cartchecked.svg" alt="cart checked"/>
                                                        <div className={`${styles.statusTextGray}`}>Pesanan<br />Diproses</div>
                                                    </div>
                                                    <div className={`${styles.linerGray}`}></div>
                                                    <div className={`${styles.statusIconGray} position-relative`}>
                                                        <img src="/images/truck.svg" alt="truck"/>
                                                        <div className={`${styles.statusTextGray}`}>Pesanan<br />Telah Dikirim</div>
                                                    </div>
                                                    <div className={`${styles.linerGray}`}></div>
                                                    <div className={`${styles.statusIconGray} position-relative`}>
                                                        <img src="/images/delivered.svg" alt="delivered"/>
                                                        <div className={`${styles.statusTextGray}`}>Pesanan<br />Selesai</div>
                                                    </div>
                                                </div>
                                            </Container>
                                        </Row>
                                    </Container>
                                </div>

                                <div className="alamat-kirim border-color-secondary p-3">
                                    <h6 className="color-primary px-3">Alamat Pengiriman</h6>
                                    <Container className="bg-gray py-3 rounded">
                                        <Row className="small">
                                            <Col xs={3}>
                                                <div>Indonesia Raya</div>
                                                <div>08119855889</div>
                                            </Col>
                                            <Col xs={9}>
                                                <div>(Alamat Kantor)</div>
                                                <div>Jalan Tampomas No. 8 Malabar</div>
                                                <div>Lengkong, Jawa Barat, Kota Bandung 40262 Indonesia</div>
                                            </Col>
                                        </Row>
                                    </Container>
                                </div>

                                <div className="jasa-pengiriman border-color-secondary p-3">
                                    <h6 className="color-primary px-3">Jasa Pengiriman</h6>
                                    <Container>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlSelect1">
                                                <Form.Control as="select">
                                                    {/* <option><img src="/images/jne.svg" alt="jne" /> JNE Regular</option> */}
                                                    <option>JNE Regular</option>
                                                    <option>JNE YES</option>
                                                    <option>TIKI Regular</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Form>
                                    </Container>
                                    <Container className="additional-info color-primary small">Additional Information</Container>
                                </div>
                                
                                <div className="pesanan border-color-secondary p-3">
                                    <h6 className="color-primary px-3">Produk Pesanan</h6>
                                    <Container>
                                        <Row>
                                            <Col xs={3}>
                                                <div className={`${styles.productWrapper}`}>
                                                    <div className={`${styles.productImgWrapper} position-relative`}>
                                                        <div className={`${styles.productImgContent}`}>
                                                            <img src="/images/placeholder-image.png" className={`${styles.imgProduct}`} alt="placeholder" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs={9}>
                                                <Row className="">
                                                    <Col sm={8} className="brand">
                                                        <h6 className="font-weight-bold">MSI Pro 16 Multitouch All in One</h6>
                                                        <div className="text-secondary mb-2 small">036AU 15.6 Multitouch <span className="ml-2">#5769355</span></div>
                                                    </Col>
                                                    <Col sm={4} className="price text-sm-right">
                                                        <h6 className="price font-weight-bold">Rp. 8.369.000</h6>
                                                        <div className="quantity font-weight-bold">x2</div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Container>
                                </div>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="d-flex flex-column border-color-secondary p-3">
                                <div className="metode mb-3">
                                    <h6 className="color-primary pb-3 border-bottom">Metode Pembayaran</h6>
                                    <div className="total-harga d-flex justify-content-between small py-1">
                                        <div className="detail-text">GO-PAY</div>
                                        <div className="detail-price"><img src="/images/gopay.svg" alt="gopay" width={60} /></div>
                                    </div>
                                </div>
                                <div className="detail">
                                    <h6 className="color-primary pb-3 border-bottom">Detail Pembayaran</h6>
                                    <div className="total-harga d-flex justify-content-between small py-1">
                                        <div className="detail-text">Total harga produk</div>
                                        <div className="detail-price">Rp. 8.369.000</div>
                                    </div>
                                    <div className="ongkos d-flex justify-content-between small py-1">
                                        <div className="detail-text">Ongkos kirim (JNE Regular)</div>
                                        <div className="detail-price">Rp. 18.000</div>
                                    </div>
                                    <div className="potongan d-flex justify-content-between small  pt-1 pb-3 border-bottom">
                                        <div className="detail-text">Potongan ongkos kirim</div>
                                        <div className="detail-price">Rp. -</div>
                                    </div>
                                    <div className="total-pembayaran d-flex justify-content-between small py-2 font-weight-bold">
                                        <div className="detail-text">Total Pembayaran</div>
                                        <div className="detail-price">Rp. 8.387.000</div>
                                    </div>
                                    <div className="payment-method-btn primary-button text-center my-3">Bayar Sekarang</div>
                                    <div className="payment-method-btn secondary-button text-center my-3 font-weight-bold">Ubah Metode Pembayaran</div>
                                </div>
                                <Container className="p-5 text-center d-none d-md-block">
                                    <img src="/images/delivery.svg" alt="cart" className="img-fluid" />
                                </Container>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        </Layout>
    );
};

export default Checkout;