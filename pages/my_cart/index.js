import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap'

import Layout from "../../components/layouts/main"

import styles from "./styles.module.css"
import { FaChevronUp, FaChevronDown, FaList, FaListAlt, FaTh, FaThLarge, FaTimesCircle, FaRegHeart, FaCheckCircle, FaShareAlt, FaShare, FaMinusSquare, FaMinus, FaRegMinusSquare, FaRegPlusSquare, FaPlusSquare, FaTimes, FaTrash, FaRegTrashAlt, FaTrashAlt } from 'react-icons/fa';
import { MdPrint } from 'react-icons/md';

const MyCart = () => {
    const route = useRouter();

    const [orderQty, setOrderQty] = useState(1)
    const [stock, setStock] = useState(5)

    const cartItems = [1, 2]

    const ratingStars = {
        size: 20,
        value: 2.5,
        edit: false
    };

    const plusOrderQty = () => {
        if (orderQty < stock) {
            setOrderQty(orderQty + 1)
        }
    }
    const minOrderQty = () => {
        if (orderQty > 1) {
            setOrderQty(orderQty - 1)
        }
    }

    return (
        <Layout>
            <section className="ads-banner bg-primary">
                <div className="banner-wrapper text-center">
                    <img src="/images/ads-banner.jpg" width="100%" alt="central ads banner" />
                </div>
            </section>
            <section className="bread-crumb py-1 bg-gray">
                <Container className="color-primary small">
                    Home {`>`} <span className="font-weight-bold">My Cart</span>
                </Container>
            </section>
            <section className="page-title py-3">
                <Container className="color-primary">
                    <span className={`${styles.pageTitle}`}>My Cart</span>
                </Container>
            </section>
            <section className="main-body py-3">
                <Container>
                    <div className={`${styles.selectRemoveItem} color-primary px-3`}>
                        <div className="select-all">
                            <input type="checkbox" id="select-all" /> <label htmlFor="select-all">Select All</label>
                        </div>
                        <div className="remove-all color-secondary">Remove All</div>
                    </div>
                    {cartItems.map(() =>
                        <Container className="border-color-secondary py-3 py-sm-4 pl-5 position-relative">
                            <div className={`${styles.checkSingleItem}`}>
                                <input type="checkbox" />
                            </div>
                            <Row>
                                <Col md={8}>
                                    <Container>
                                        <Row>
                                            <Col xs={4}>
                                                <div className={`${styles.productWrapper}`}>
                                                    <div className={`${styles.productImgWrapper} position-relative`}>
                                                        <div className={`${styles.productImgContent}`}>
                                                            <img src="/images/placeholder-image.png" className={`${styles.imgProduct}`} alt="placeholder" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs={8}>
                                                <h5 className="color-primary font-weight-bold">MSI Pro 16 Multitouch All in One</h5>
                                                <div className="text-secondary mb-2">036AU 15.6 Multitouch <span className="ml-2">#5769355</span></div>
                                                <div className={`text-success mb-3`}>
                                                    <FaCheckCircle className="mr-1" /> in Stock
                                                    </div>
                                                <h5 className="price color-primary font-weight-bold">Rp. 8.369.000</h5>
                                            </Col>
                                        </Row>
                                    </Container>
                                </Col>
                                <Col md={4} className={`d-flex align-items-end flex-column`}>
                                    <div className="order-qty d-flex w-100">
                                        <div className={`${styles.plusMinus} color-primary no-select mr-auto`}>
                                            {orderQty > 1 ?
                                                <FaMinusSquare onClick={minOrderQty} /> :
                                                <FaRegMinusSquare onClick={minOrderQty} />
                                            }
                                            <div className={`${styles.orderQuantity}`}>{orderQty}</div>
                                            {orderQty < stock ?
                                                <FaPlusSquare onClick={plusOrderQty} /> :
                                                <FaRegPlusSquare onClick={plusOrderQty} />
                                            }
                                        </div>
                                        <div className={`${styles.priceInPopup} color-primary`}>Rp. 8.369.000</div>
                                    </div>
                                    <div className={`${styles.binIcon} mt-auto`}>
                                        <div className="removeSingleItem rounded border-color-primary color-primary py-1 px-3">
                                            Remove <FaTrashAlt className="color-primary" />
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    )}
                    <Container className={`${styles.subTotal} border-color-secondary py-3 py-sm-4 pl-5`}>
                        <Row className="">
                            <Col xs={6} sm={8} className="totalItemText">Total Item</Col>
                            <Col xs={6} sm={4} className="totalItemText text-right font-weight-bold">2</Col>
                        </Row>
                        <Row className="">
                            <Col xs={6} sm={8} className="totalItemText">Subtotal</Col>
                            <Col xs={6} sm={4} className="totalItemText text-right font-weight-bold">Rp. 8.369.000</Col>
                        </Row>
                    </Container>
                </Container>
            </section>
        </Layout>
    );
};

export default MyCart;
