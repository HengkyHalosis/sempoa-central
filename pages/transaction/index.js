import React, { useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { FaArrowLeft, FaLongArrowAltLeft } from 'react-icons/fa';
import Layout from '../../components/layouts/main'
import styles from './styles.module.css'
import Link from 'next/link'

const Checkout = () => {

    return (
        <Layout title="Status Order">
            <section className="py-4">
                <Container>
                    <Row>
                        <Col md={4}>
                            <div className="d-flex flex-column border-color-secondary py-3">
                                <div className="sidebar-top mb-3 border-bottom">
                                    <Container className="p-4 text-center color-primary">
                                        <img src="/images/no-user.svg" alt="cart" className="img-fluid" />
                                        <div className="user-name mt-2">Indonesia Raya</div>
                                        <div className="user-email">indonesiaraya@gmail.com</div>
                                    </Container>
                                </div>
                                <div className="sidebar-lists py-3">
                                    <Container className="color-primary">
                                        <Row className="py-2 py-md-3 pointer">
                                            <Col xs={4} className="text-center"><img src="/images/akun.svg" alt="akun" /></Col>
                                            <Col xs={8} className="d-flex align-items-center pl-0"><h5 className="m-0">Akun</h5></Col>
                                        </Row>
                                        <Row className="py-2 py-md-3 pointer">
                                            <Col xs={4} className="text-center"><img src="/images/transaksi.svg" alt="transaksi" /></Col>
                                            <Col xs={8} className="d-flex align-items-center pl-0"><h5 className="m-0">Transaksi</h5></Col>
                                        </Row>
                                        <Row className="py-2 py-md-3 pointer">
                                            <Col xs={4} className="text-center"><img src="/images/alamat.svg" alt="alamat" /></Col>
                                            <Col xs={8} className="d-flex align-items-center pl-0"><h5 className="m-0">Alamat</h5></Col>
                                        </Row>
                                        <Row className="py-2 py-md-3 pointer">
                                            <Col xs={4} className="text-center"><img src="/images/wishlist.svg" alt="wishlist" /></Col>
                                            <Col xs={8} className="d-flex align-items-center pl-0"><h5 className="m-0">Wishlist</h5></Col>
                                        </Row>
                                        <Row className="py-2 py-md-3 pointer">
                                            <Col xs={4} className="text-center"><img src="/images/inquiries.svg" alt="inquiries" /></Col>
                                            <Col xs={8} className="d-flex align-items-center pl-0"><h5 className="m-0">Inquiries</h5></Col>
                                        </Row>
                                        <Row className="py-2 py-md-3 pointer">
                                            <Col xs={4} className="text-center"><img src="/images/notifikasi.svg" alt="notifikasi" /></Col>
                                            <Col xs={8} className="d-flex align-items-center pl-0"><h5 className="m-0">Notifikasi</h5></Col>
                                        </Row>
                                        <Row className="py-2 py-md-3 pointer">
                                            <Col xs={4} className="text-center"><img src="/images/logout.svg" alt="logout" /></Col>
                                            <Col xs={8} className="d-flex align-items-center pl-0"><h5 className="m-0">Logout</h5></Col>
                                        </Row>
                                    </Container>
                                </div>
                            </div>
                        </Col>

                        <Col md={8}>
                            <div className="">
                                <div className="border-color-secondary rounded-top">
                                    <Container className="p-3 bg-gray">
                                        <Row className="small">
                                            <Col xs={5}>
                                                <div className="orderNumber color-primary">
                                                    <h6 className="color-primary m-0">
                                                        No Pesanan : 8381157
                                                    </h6>
                                                </div>
                                            </Col>
                                            <Col xs={7}>
                                                <div className="orderTime color-primary text-md-right">
                                                    Tanggal Pemesanan : 24 November 2020, 11:15 WIT
                                                </div>
                                            </Col>
                                        </Row>
                                    </Container>
                                </div>

                                <div className="pesanan border-color-secondary py-4">
                                    <Container>
                                        <Row>
                                            <Col xs={3} md={2}>
                                                <div className={`${styles.productWrapper}`}>
                                                    <div className={`${styles.productImgWrapper} position-relative`}>
                                                        <div className={`${styles.productImgContent}`}>
                                                            <img src="/images/placeholder-image.png" className={`${styles.imgProduct}`} alt="placeholder" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs={9} md={6} className="pl-md-0">
                                                <Row className="">
                                                    <Col sm={8} className="brand">
                                                        <h6 className="font-weight-bold small">MSI Pro 16 Multitouch All in One</h6>
                                                        <div className="text-secondary mb-2 small">036AU 15.6 Multitouch <span className="ml-2">#5769355</span></div>
                                                    </Col>
                                                    <Col sm={4} className="price text-sm-right pr-md-0">
                                                        <h6 className="price font-weight-bold small">Rp. 8.369.000</h6>
                                                        <div className="quantity font-weight-bold">x2</div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col sm={9} md={4} className="pl-md-0 offset-3 offset-md-0">
                                                <Row className="">
                                                    <Col className="jasa-pengiriman text-center">
                                                        <h6 className="font-weight-bold small">GO-PAY</h6>
                                                    </Col>
                                                    <Col className="order-status text-center text-warning">
                                                        <h6 className="font-weight-bold small text-center">Menunggu Pembayaran</h6>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Container>
                                </div>

                                
                                <div className="border-color-secondary rounded-bottom">
                                    <Container className="p-3">
                                        <Row className="small">
                                            <Col xs={4} className="d-flex align-items-center">
                                                <div className="orderNumber color-primary">
                                                    <h6 className="color-primary m-0">
                                                        Lihat Detail Pesanan
                                                    </h6>
                                                </div>
                                            </Col>
                                            <Col xs={8} className="d-flex flex-wrap justify-content-end align-items-center">
                                                <div className="orderTime color-primary mr-2 mb-2 mb-lg-0">
                                                    Bayar sebelum 25 Nov 2020, 11.15 WIT
                                                </div>
                                                <div className="primary-button btn-sm">Bayar Sekarang</div>
                                            </Col>
                                        </Row>
                                    </Container>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        </Layout>
    );
};

export default Checkout;