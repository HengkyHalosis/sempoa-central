import Document, { Html, Head, Main, NextScript } from 'next/document'
export const config = { amp: true }

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument