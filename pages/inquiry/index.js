import { useState } from 'react'
import Layout from "../../components/layouts/main"
import { Container, Form, Row, Col, Button } from "react-bootstrap"
import Select from 'react-select';
import { InputText, InputTextArea, InputSelect } from '../../components/formik/fields'
import { Formik, useField, useFormikContext } from "formik";
import * as Yup from "yup";
import router, { useRouter } from 'next/router';
import Multiselect from '../../components/formik/multiselect'
const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
];

export default function Home() {
    const [selectedOption, setSelectedOption] = useState(null)
    const [selectedProduct, setSelectedProduct] = useState(null)

    const options = [
        { value: 'Product', label: 'Product' },
        { value: 'Metode Pembayaran', label: 'Metode Pembayaran' },
        { value: 'Pengiriman', label: 'Pengiriman' },
        { value: 'Pemesanan Jumlah Besar', label: 'Pemesanan Jumlah Besar' },
        { value: 'Pengambilan Barang', label: 'Pengambilan Barang' },
    ];

    const products = [
        { value: 'Face Shield', label: 'Face Shield' },
        { value: 'Face Shield Kacamata', label: 'Face Shield Kacamata' },
        { value: 'Face Shield Anak', label: 'Face Shield Anak' },
        { value: 'Helm Full Face', label: 'Helm Full Face' },
    ];

    const handleChange = (selected) => {
        setSelectedOption(selected)
        console.log("option selected: ", selected);
    }

    const handleChangeProducts = (product) => {
        setSelectedProduct(product)
        console.log("product selected: ", product);
    }

    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    return (
        <Layout title="Inquiry">
            <section className="py-4 py-sm-5">
                <Container>
                    <Row>
                        <Col sm={8} md={7}>
                            <h5 className="color-primary font-weight-bold mb-4">Please fill your contact details</h5>
                            <Formik
                                initialValues={{
                                    inquiry: "",
                                    name: "",
                                    email: "",
                                    phone: 0,
                                    products: [],
                                    question: "",
                                }}
                                validationSchema={Yup.object({
                                    inquiry: Yup.string()
                                        .required("Required"),
                                    name: Yup.string()
                                        .min(8, "Must be 8 characters minimum")
                                        .max(50, "Must be 50 characters max")
                                        .required("Required"),
                                    email: Yup.string()
                                        .email("Invalid email addresss")
                                        .required("Required"),
                                    phone: Yup.string()
                                        .matches(phoneRegExp, 'Phone number is not valid')
                                        .required("Required"),
                                    products: Yup.array()
                                        .min(1, "Pick at least 1 tags")
                                        .of(
                                        Yup.object().shape({
                                            label: Yup.string().required(),
                                            value: Yup.string().required()
                                        })
                                        )

                                })}
                                onSubmit={(values, { setSubmitting }) => {
                                    const payload = {
                                      ...values,
                                      products: values.products.map(t => t.value)
                                    };
                                    setTimeout(() => {
                                      console.log(payload)
                                      setSubmitting(false);
                                    }, 3000);
                                }}
                            >
                                {({ values, isValid, dirty, touched, handleChange, handleSubmit, handleBlur, handleReset, setFieldValue, setFieldTouched, errors, validateForm, isSubmitting }) => (
                                    <Form onSubmit={handleSubmit}>
                                        <Form.Group controlId="formBasicInquiry">
                                            <InputSelect name="inquiry" label="Inquiry Subject" placeholder="select inquiry">
                                                <option value="">Select Inquiry</option>
                                                {options.map((option) =>
                                                    <option key={option.value} value={option.value}>{option.label}</option>
                                                )}
                                            </InputSelect>
                                        </Form.Group>

                                        <Form.Group controlId="formBasicName">
                                            <InputText label="Name" name="name" type="text" placeholder="your name" />
                                        </Form.Group>

                                        <Form.Group controlId="formBasicEmail">
                                            <InputText label="Email" name="email" type="email" placeholder="your.email@mail.com" />
                                        </Form.Group>

                                        <Form.Group controlId="formBasicPhone">
                                            <InputText label="Phone Number" name="phone" type="text" placeholder="Your phone number" />
                                        </Form.Group>

                                        <Form.Group controlId="formBasicSelection">
                                            <Form.Label className="color-primary">Selection</Form.Label>
                                            <Multiselect
                                                name="products"
                                                instanceId="product-central"
                                                options={products}
                                                value={values.products}
                                                onChange={setFieldValue}
                                                onBlur={setFieldTouched}
                                                error={errors.products}
                                                touched={touched.products}
                                                dirty={dirty.products}
                                            />
                                        </Form.Group>

                                        <Form.Group controlId="formBasicQustion">
                                            <InputTextArea name="question" rows={3} id="question" htmlFor="question" label="Question" placeholder="Your question?" />
                                        </Form.Group>

                                        <button className="btn-sm primary-button text-center my-3 col-md-5 mx-auto text-center d-block border-0 shadow-none" type="submit" disabled={!(isValid && dirty && !isSubmitting)}>
                                            Submit
                                        </button>
                                        {/* <pre>
                                            {JSON.stringify(values, null, 2)}
                                        </pre> */}
                                    </Form>
                                )}
                            </Formik>
                        </Col>
                        <Col sm={4} md={5} className="d-none d-sm-flex justify-content-center align-items-center">
                            <img src="/images/inquiry.svg" alt="inquiry page image" className="img-fluid w-75" />
                        </Col>
                    </Row>
                </Container>
            </section>
        </Layout>
    )
}
