import router, { useRouter } from 'next/router';
import React, { useState } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap'

import Layout from "../../../../../components/layouts/main"

import styles from "./styles.module.css"
import StylesRelatedProduct from '../../../../../components/componentStyles.module.css'
import { FaChevronUp, FaChevronDown, FaList, FaListAlt, FaTh, FaThLarge, FaTimesCircle, FaRegHeart, FaCheckCircle, FaShareAlt, FaShare, FaMinusSquare, FaMinus, FaRegMinusSquare, FaRegPlusSquare, FaPlusSquare, FaTimes, FaTrash, FaRegTrashAlt } from 'react-icons/fa';
import ReactStars from "react-rating-stars-component";
import { MdPrint } from 'react-icons/md';
import Swiper from 'react-id-swiper';
import 'swiper/swiper.min.css';


const DetailProduct = () => {
    const route = useRouter();

    const [orderQty, setOrderQty] = useState(1)
    const [stock, setStock] = useState(5)
    const [addtoCartPopup, setAddtoCartPopup] = useState(false)

    const categories = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    const ratingStars = {
        size: 20,
        value: 2.5,
        edit: false
    };

    const plusOrderQty = () => {
        if (orderQty < stock) {
            setOrderQty(orderQty + 1)
        }
    }
    const minOrderQty = () => {
        if (orderQty > 1) {
            setOrderQty(orderQty - 1)
        }
    }

    const params = {
        // slidesPerView: 3.5,
        spaceBetween: 10,
        // freeMode: true
    }

    return (
        <Layout>
            <section className="bread-crumb py-1 bg-gray">
                <Container className="color-primary small">
                    Home {`>`} Product Category {`>`} {route.query.category_one} {`>`} {route.query.category_two} {`>`} <span className="font-weight-bold">{route.query.id}</span>
                </Container>
            </section>
            <section className="page-title py-3">
                <Container className="color-primary">
                    <div className={`${styles.pageTitle}`}>MSI Pro 16 Multitouch All in One</div>
                    <div className="text-secondary">036AU 15.6 Multitouch #5769355</div>
                </Container>
            </section>

            <section className="product-details py-3">
                <Container>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <Row>
                                    <Col xs={3} className="pl-0">
                                        <div className="d-flex flex-column">
                                            {["image1", "image2", "image3"].map((image, index) =>
                                                <div className="image-thumbnail pb-3">
                                                    <div className={`${styles.productWrapper}`}>
                                                        <div className={`${styles.productImgWrapperThumb} position-relative`}>
                                                            <div className={`${styles.productImgContent}`}>
                                                                <img src="/images/placeholder-image.png" className={`${styles.imgProduct}`} alt="placeholder" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                    </Col>
                                    <Col xs={9} className="position-relative">
                                        <div className={`${styles.productWrapper}`}>
                                            <div className={`${styles.productImgWrapper} position-relative`}>
                                                <div className={`${styles.filterFlag}`}>New Release</div>
                                                <div className={`${styles.productImgContent}`}>
                                                    <img src="/images/placeholder-image.png" className={`${styles.imgProduct}`} alt="placeholder" />
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </Col>
                        <Col md={4} className="px-3 px-sm-0">
                            <div className="top-bar d-flex flex-wrap justify-content-between color-secondary">
                                <div className={`text-success`}>
                                    <FaCheckCircle className="mr-1" /> in Stock
                                </div>
                                <div className="share-and-pring">
                                    <span className="pr-2"><FaShare className={`${styles.shareIcon}`} />Share</span>
                                    <span className="pl-2"><MdPrint className="mr-1" />Print</span>
                                </div>
                            </div>
                            <div className={`${styles.price} color-primary py-3 py-md-4`}>Rp. 8.369.000</div>
                            <div className="order-qty d-flex justify-content-between flex-wrap align-items-center">
                                <div className={`${styles.plusMinus} color-primary no-select mr-3`}>
                                    {orderQty > 1 ?
                                        <FaMinusSquare onClick={minOrderQty} /> :
                                        <FaRegMinusSquare onClick={minOrderQty} />
                                    }
                                    <div className={`${styles.orderQuantity}`}>{orderQty}</div>
                                    {orderQty < stock ?
                                        <FaPlusSquare onClick={plusOrderQty} /> :
                                        <FaRegPlusSquare onClick={plusOrderQty} />
                                    }
                                </div>
                                <div className={`${styles.addToCartBtn} primary-button`} onClick={() => setAddtoCartPopup(true)}>Add to cart</div>
                            </div>

                            <Container className="wishlist-inquiry-btn text-center mt-4 mt-md-5">
                                <Row>
                                    <Col className="pl-0 pr-2">
                                        <div className="secondary-button small">Add to Wishlist</div>
                                    </Col>
                                    <Col className="pr-0 pl-2">
                                        <div className="secondary-button small">Add to Inquiries</div>
                                    </Col>
                                </Row>
                            </Container>
                        </Col>
                    </Row>
                </Container>
            </section>

            <section className="description pt-3 pb-5">
                <Container className="color-primary">
                    <div className="section-title d-flex justify-content-center align-items-center mb-4">
                        <div className={`${styles.liner}`}></div>
                        <div className={`${styles.sectionTitle}`}>Description</div>
                        <div className={`${styles.liner}`}></div>
                    </div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam voluptates, natus debitis deserunt minima officiis qui in, perspiciatis mollitia expedita atque aspernatur architecto, sed culpa explicabo aperiam amet doloremque incidunt?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error illum numquam fugit sequi, odio tenetur molestias accusantium. Quae repudiandae nemo blanditiis, eveniet, dignissimos amet qui illo fuga sit, numquam molestias.</p>
                </Container>
            </section>

            <section className="related-product pt-3 pb-5">
                <Container className="color-primary">
                    <div className="section-title d-flex justify-content-center align-items-center mb-4">
                        <div className={`${styles.liner}`}></div>
                        <div className={`${styles.sectionTitle}`}>Related Product</div>
                        <div className={`${styles.liner}`}></div>
                    </div>
                    <Row className="product-lists-wrapper">
                        {[1, 2, 3, 4, 5, 6].map((category, idx) => (
                            <Col xs={6} sm={4} md={3} lg={2} key={idx} className="product-item mb-4 px-2 small">
                                <div className={`${StylesRelatedProduct.productWrapper}`}>
                                    <div className={`${StylesRelatedProduct.productImgWrapper}`}>
                                        <div className={`${StylesRelatedProduct.productImgContent}`}>
                                            <img src="/images/placeholder-image.png" className={`${StylesRelatedProduct.imgProduct}`} alt="placeholder" />
                                        </div>
                                        <div className={`${styles.stockStatus}`}>
                                            <FaCheckCircle className="text-success" /> in Stock
                                        </div>
                                    </div>
                                    <div className={`${StylesRelatedProduct.productDetails} bg-gray`}>
                                        <Container>
                                            <h6 className="color-primary font-weight-bold mb-0">Product Name</h6>
                                            <div className="product-description text-secondary">Product Descriptions</div>
                                            <div className="product-description text-secondary">All in One</div>
                                            <div className="product-price color-primary font-weight-bold mb-2">
                                                Rp. 389.000
                                            </div>
                                            <div className={`${styles.addToCart} m-0`}>
                                                <div className="px-2 bg-gray border-color-primary mr-1 rounded">
                                                    <FaRegHeart className={`${styles.heartIcon} color-primary`} />
                                                </div>
                                                <div className="btn-addtocart button-hover px-2 bg-color-primary flex-grow-1 text-center text-white rounded">Add to Cart</div>
                                            </div>
                                        </Container>
                                    </div>
                                </div>
                            </Col>
                        ))}
                    </Row>
                </Container>
            </section>
            {addtoCartPopup ?
                <div className={`${styles.modalAddtoCartWrapper}`}>
                    <div className={`${styles.modalAddToCartContainer}`}>
                        <FaTimes className={`${styles.closeModal}`} onClick={() => setAddtoCartPopup(false)} />
                        <h2 className={`${styles.modalTitle}`}>1 Item Added to Your Cart</h2>
                        <div className={`${styles.modalAddToCartContent}`}>
                            <Container className="modal-body pb-3 border-bottom">
                                <Row>
                                    <Col md={7}>
                                        <Container>
                                            <Row>
                                                <Col xs={4}>
                                                    <div className={`${styles.productWrapper}`}>
                                                        <div className={`${styles.productImgWrapper} position-relative`}>
                                                            <div className={`${styles.productImgContent}`}>
                                                                <img src="/images/placeholder-image.png" className={`${styles.imgProduct}`} alt="placeholder" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col xs={8}>
                                                    <h5 className="color-primary font-weight-bold">MSI Pro 16 Multitouch All in One</h5>
                                                    <div className="text-secondary mb-2">036AU 15.6 Multitouch <span className="ml-2">#5769355</span></div>
                                                    <div className={`text-success mb-3`}>
                                                        <FaCheckCircle className="mr-1" /> in Stock
                                                    </div>
                                                    <h5 className="price color-primary font-weight-bold">Rp. 8.369.000</h5>
                                                </Col>
                                            </Row>
                                        </Container>
                                    </Col>
                                    <Col md={5} className={``}>
                                        <div className={`${styles.cartButton} primary-button ml-auto`}>View Cart</div>
                                        <div className={`${styles.cartButton} secondary-button ml-auto`}>Update Cart</div>

                                        <div className="order-qty d-flex justify-content-between flex-wrap align-items-center">
                                            <div className={`${styles.plusMinus} color-primary no-select mr-3`}>
                                                {orderQty > 1 ?
                                                    <FaMinusSquare onClick={minOrderQty} /> :
                                                    <FaRegMinusSquare onClick={minOrderQty} />
                                                }
                                                <div className={`${styles.orderQuantity}`}>{orderQty}</div>
                                                {orderQty < stock ?
                                                    <FaPlusSquare onClick={plusOrderQty} /> :
                                                    <FaRegPlusSquare onClick={plusOrderQty} />
                                                }
                                            </div>
                                            <div className={`${styles.priceInPopup} color-primary`}>Rp. 8.369.000</div>
                                            <div className={`${styles.binIcon}`}><FaRegTrashAlt className="color-primary" /></div>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                            <Container className="total-order pb-4 border-bottom">
                                <Row className="py-3">
                                    <Col md={5} className={`offset-md-7 d-flex justify-content-between color-primary`}>
                                        <div className="text-total"><h5 className="font-weight-bold">Total</h5></div>
                                        <div className="text-total"><h5 className="font-weight-bold">Rp. 8.368.000</h5></div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col><div className="secondary-button font-weight-bold text-center">Continue Shopping</div></Col>
                                    <Col><div className="primary-button text-center">Proceed To Checkout</div></Col>
                                </Row>
                            </Container>
                            <Container className="recommended py-3">
                                <h5 className="color-primary font-weight-bold">Recommended Accesories</h5>
                                <div className="product-swiper">
                                    <Swiper {...params}>
                                        {[1, 2, 3, 4, 5, 6].map((category, idx) => (
                                            <div key={idx} className="related-product-item col-8 col-sm-5 col-md-3 mb-4 px-2 small">
                                                <div className={`${StylesRelatedProduct.productWrapper}`}>
                                                    <div className={`${StylesRelatedProduct.productImgWrapper}`}>
                                                        <div className={`${StylesRelatedProduct.productImgContent}`}>
                                                            <img src="/images/placeholder-image.png" className={`${StylesRelatedProduct.imgProduct}`} alt="placeholder" />
                                                        </div>
                                                        <div className={`${styles.stockStatus}`}>
                                                            <FaCheckCircle className="text-success" /> in Stock
                                                        </div>
                                                    </div>
                                                    <div className={`${StylesRelatedProduct.productDetails} bg-gray`}>
                                                        <Container>
                                                            <h6 className="color-primary font-weight-bold mb-0">Product Name</h6>
                                                            <div className="product-description text-secondary">Product Descriptions</div>
                                                            <div className="product-description text-secondary">All in One</div>
                                                            <div className="product-price color-primary font-weight-bold mb-2">
                                                                Rp. 389.000
                                                            </div>
                                                            <div className={`${styles.addToCart} m-0`}>
                                                                <div className="px-2 bg-gray border-color-primary mr-1 rounded">
                                                                    <FaRegHeart className={`${styles.heartIcon} color-primary`} />
                                                                </div>
                                                                <div className="btn-addtocart button-hover px-2 bg-color-primary flex-grow-1 text-center text-white rounded">Add to Cart</div>
                                                            </div>
                                                        </Container>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                        <div className={`${styles.seeAllLink} my-auto pointer color-primary`} onClick={() => {console.log("see all accessories")}}>See All Accesories</div>
                                    </Swiper>
                                </div>
                            </Container>
                        </div>
                    </div>
                </div>
                : null}
            <style jsx>
                {`
                    @media(max-width: 991px) {
                        .btn-addtocart {
                            font-size: .7rem;
                            line-height: 1.2rem;
                        }
                    }
                    @media(max-width: 575px) {
                        .btn-addtocart {
                            font-size: 1rem;
                            line-height: 1.2rem;
                        }
                    }
                    @media(max-width: 388px) {
                        .btn-addtocart {
                            font-size: 3vw;
                            line-height: 1.2rem;
                        }
                    }
                `}
            </style>
        </Layout>
    );
};

export default DetailProduct;
