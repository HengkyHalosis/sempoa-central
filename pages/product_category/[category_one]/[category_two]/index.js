import router, { useRouter } from 'next/router';
import React, { useState } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap'

import Layout from "../../../../components/layouts/main"

import styles from "./styles.module.css"
import { FaChevronUp, FaChevronDown, FaList, FaListAlt, FaTh, FaThLarge, FaTimesCircle, FaRegHeart, FaCheckCircle } from 'react-icons/fa';
import ReactStars from "react-rating-stars-component";


const CategoryTwo = () => {
    const route = useRouter();
    
    const[layout, setLayout] = useState("grid")
    const[showCategorySelections, setShowCategorySelections] = useState(true)
    const[showBrandsSelections, setShowBrandsSelections] = useState(true)
    const[showBestDealsSelections, setShowBestDealsSelections] = useState(false)
    const[showPricesSelections, setShowPriceSelections] = useState(false)
    const[showCustRatingSelections, setShowCustRatingSelections] = useState(false)

    const categories = [1,2,3,4,5,6,7,8,9]

    const ratingStars = {
        size: 20,
        value: 2.5,
        edit: false
      };
    return (
        <Layout>
            <section className="ads-banner bg-primary">
                <div className="banner-wrapper text-center">
                    <img src="/images/ads-banner.jpg" width="100%" alt="central ads banner"/>
                </div>
            </section>
            <section className="bread-crumb py-1 bg-gray">
                <Container className="color-primary small">
                    Home {`>`} Product Category {`>`} <span className="">{route.query.category_one}</span> {`>`} <span className="font-weight-bold">{route.query.category_two}</span>
                </Container>
            </section>
            <section className="page-title py-3">
                <Container className="color-primary">
                    <span className={`${styles.pageTitle}`}>{route.query.category_one}</span> <span className="small">items 1 - 30 of 1296</span>
                </Container>
            </section>
            <section className="filter-sort-bar bg-gray">
                <Container className="d-flex justify-content-between align-items-center">
                    <div className="d-flex filter-wrapper flex-wrap py-2">
                        <div className="filter-by mr-2">
                            <span className="mr-2">Filter By</span>
                            <select>
                                <option value="new release">New Release</option>
                                <option value="old release">Old Release</option>
                            </select>
                        </div>
                        <div className="sort-by mr-2">
                            <span className="mr-2">Sort By</span>
                            <select>
                                <option value="best">Best Seller</option>
                                <option value="worst">Worst Seller</option>
                            </select>
                        </div>
                        <div className="perpage">
                            <span className="mr-2">Show</span>
                            <select>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                    </div>
                    <div className="layout-view d-flex justify-content-end align-items-center">
                        <span className="mr-2">Change View</span>
                        <div className={layout === "grid" ? "grid-view layout bg-color-primary text-white small text-center pointer" : "grid-view layout small color-primary text-center pointer"}
                        onClick={() => setLayout("grid")}
                        ><FaTh /><br />Grid</div>
                        <div className={layout !== "grid" ? "grid-view layout bg-color-primary text-white small text-center pointer" : "grid-view layout small color-primary text-center pointer"}
                        onClick={() => setLayout("list")}
                        ><FaList /><br />List</div>
                    </div>
                </Container>
            </section>

            <section className="main-body py-2">
                <Container>
                    <Row>
                        <Col md={2} className="sidebar bg-gray rounded py-2 mb-3">
                            <div className="category-selections border-bottom py-2">
                                <div className="selection-title d-flex justify-content-between align-items-center text-secondary"
                                onClick={() => setShowCategorySelections(!showCategorySelections)}
                                >Category {showCategorySelections ? <FaChevronUp /> : <FaChevronDown />}</div>
                                {showCategorySelections ? 
                                <ul className="small list-unstyled my-2">
                                    <li className="mb-1">Laptop (18)</li>
                                    <li className="mb-1">Notebook (18)</li>
                                    <li className="mb-1">Handphone (23)</li>
                                    <li className="mb-1">Accessories (7)</li>
                                </ul>
                                : null }
                            </div>

                            <div className="brands-selections border-bottom py-2">
                                <div className="selection-title d-flex justify-content-between align-items-center text-secondary"
                                onClick={() => setShowBrandsSelections(!showBrandsSelections)}
                                >Brands {showBrandsSelections ? <FaChevronUp /> : <FaChevronDown />}</div>
                                {showBrandsSelections ? 
                                    <ul className="small list-unstyled my-2">
                                        <li>
                                        <input type="checkbox" id="lenovo" value="lenovo" onClick={(e) => console.log(e.target.value)} /> <label htmlFor="lenovo">Lenovo</label>
                                        </li>
                                        <li>
                                        <input type="checkbox" id="msi" value="msi" onClick={(e) => console.log(e.target.value)} /> <label htmlFor="msi">MSI</label>
                                        </li>
                                        <li>
                                        <input type="checkbox" id="acer" value="acer" onClick={(e) => console.log(e.target.value)} /> <label htmlFor="acer">Acer</label>
                                        </li>
                                        <li>
                                        <input type="checkbox" id="samsung" value="samsung" onClick={(e) => console.log(e.target.value)} /> <label htmlFor="samsung">Samsung</label>
                                        </li>
                                    </ul>
                                : null }
                            </div>
                            
                            {/* <div className="best-deals-selections border-bottom py-2">
                                <div className="selection-title d-flex justify-content-between align-items-center text-secondary"
                                onClick={() => setShowBestDealsSelections(!showBestDealsSelections)}
                                >Best Deals {showBestDealsSelections ? <FaChevronUp /> : <FaChevronDown />}</div>
                                {showBestDealsSelections ? 
                                <ul className="small list-unstyled my-2">
                                    <li className="mb-1">Best deals 1</li>
                                    <li className="mb-1">Best deals 2</li>
                                    <li className="mb-1">Best deals 3</li>
                                </ul>
                                : null }
                            </div> */}

                            <div className="price-selections border-bottom py-2">
                                <div className="selection-title d-flex justify-content-between align-items-center text-secondary"
                                onClick={() => setShowPriceSelections(!showPricesSelections)}
                                >Price {showPricesSelections ? <FaChevronUp /> : <FaChevronDown />}</div>
                                {showPricesSelections ? 
                                <ul className="small list-unstyled my-2">
                                    <li className="mb-1">Prices 1</li>
                                    <li className="mb-1">Prices 2</li>
                                    <li className="mb-1">Prices 3</li>
                                </ul>
                                : null }
                            </div>
                            
                            {/* <div className="rating-selections border-bottom py-2">
                                <div className="selection-title d-flex justify-content-between align-items-center text-secondary"
                                onClick={() => setShowCustRatingSelections(!showCustRatingSelections)}
                                >Rating {showCustRatingSelections ? <FaChevronUp /> : <FaChevronDown />}</div>
                                {showCustRatingSelections ? 
                                <ul className="small list-unstyled my-2">
                                    <li className="mb-1">Rating 1</li>
                                    <li className="mb-1">Rating 2</li>
                                    <li className="mb-1">Rating 3</li>
                                </ul>
                                : null }
                            </div> */}
                        </Col>
                        <Col md={10} className="products-wrapper">
                            <div className="filters-buttons font-weight-bold d-flex mb-2">
                                <div className="filter1 filter-item">Laptop (128) <FaTimesCircle className="text-danger" /></div>
                                <div className="filter2 filter-item">HP/Compaq (57) <FaTimesCircle className="text-danger" /></div>
                                <div className="clear-filter filter-item">Clear Filter</div>
                            </div>
                            <Row className="product-lists-wrapper">
                            {categories.map((category, idx) => (
                                <Col xs={6} sm={4} md={3} key={idx} className="product-item mb-4 px-2 position-relative pointer" onClick={() => router.push(`/product_category/${route.query.category_one}/${route.query.category_two}/${category}`)}>

                                    <div className={`${styles.filterFlag}`}>New Release</div>

                                    <div className={`${styles.productWrapper}`}>
                                        <div className={`${styles.productImgWrapper} position-relative`}>
                                            <div className={`${styles.productImgContent}`}>
                                                <img src="/images/placeholder-image.png" className={`${styles.imgProduct}`} alt="placeholder"/>
                                            </div>
                                            <div className={`${styles.stockStatus}`}>
                                                <FaCheckCircle className="text-success" /> in Stock
                                            </div>
                                        </div>
                                        <div className={`${styles.productDetails} bg-gray`}>
                                            <Container>
                                                <div className="rating-stars d-flex align-items-center">
                                                    <ReactStars {...ratingStars} /> <span className="reviews-count text-secondary small ml-1">Reviews(4)</span>
                                                </div>
                                                <h6 className="color-primary font-weight-bold mb-0">Product Name</h6>
                                                <div className="product-description text-secondary">Product Descriptions</div>
                                                <div className="product-description text-secondary">All in One</div>
                                                <div className="product-price color-primary font-weight-bold mb-2">
                                                    Rp. 389.000
                                                </div>
                                                <div className={`${styles.addToCart} m-0`}>
                                                    <div className="px-2 bg-gray border-color-primary mr-1 rounded">
                                                        <FaRegHeart className="color-primary" />
                                                    </div>
                                                    <div className="btn-addtocart button-hover px-2 bg-color-primary flex-grow-1 text-center text-white rounded">Add to Cart</div>
                                                </div>
                                            </Container>
                                        </div>
                                    </div>
                                </Col>
                            ))}
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </section>
            
            <style jsx>{`
                .filter-sort-bar select {
                    color: #666;
                    background: none;
                    border-radius: 5px;
                    border: solid 1px #ccc;
                    padding: 0 .3rem;
                }
                .layout {
                    padding: .5rem;
                }
                .selection-title {
                    color: 
                }
                .filter-item {
                    margin-right: .5rem;
                    border: solid 1px #ccc;
                    padding: 0 .7rem;
                    border-radius: 5px;
                }
            `}</style>
        </Layout>
    );
};

export default CategoryTwo;
