import { Container } from 'react-bootstrap';
import CheckoutLayout from '../../components/layouts/checkoutLayout'
import Link from 'next/link'

const Confirmation = () => {
    return (
        <CheckoutLayout confirm="delivered" title="Confirmation">
            <section className="py-4">
                <Container className="border-color-secondary py-5">
                    <div className="img-thankyou text-center px-4 position-relative mb-4">
                        <img src="/images/thankyou.svg" alt="central thank you image" className="img-fluid" />
                        <div className="notif-text">
                            <h6>Pembayaran telah diterima</h6>
                            <h6>Pesanan anda akan segera kami proses</h6>
                        </div>
                    </div>
                    <Container className="py-2">
                        <div className="d-flex flex-wrap justify-content-between font-weight-bold bg-gray px-3 py-2">
                            <div className="orderNumberText">No Pesanan</div>
                            <div className="orderNumber">8381157</div>
                        </div>
                    </Container>
                    <Container className="py-2">
                        <div className="px-3 pb-3 border-bottom">
                            <div className="paymentMethodText font-weight-bold">Metode Pembayaran</div>
                            <div className="d-flex flex-wrap justify-content-between font-weight-bold py-2">
                                <div className="paymentMethod">GO-PAY</div>
                                <div className="orderNumber"><img src="/images/gopay.svg" alt="gopay"/></div>
                            </div>
                        </div>
                    </Container>
                    <Container className="py-2">
                        <div className="px-3">
                            <div className="orderNumberText font-weight-bold">Total Pembayaran</div>
                            <div className="orderNumberText font-weight-bold">Rp. 8.387.000</div>
                        </div>
                    </Container>
                    <Container className="py-2">
                        <Link href="/status-order">
                            <div className="btn-block primary-button font-weight-bold text-center">Lihat Detail Pesanan</div>
                        </Link>
                    </Container>
                </Container>
            </section>

        </CheckoutLayout>
    );
};

export default Confirmation;