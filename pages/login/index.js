import Head from 'next/head'
import { Container, Row, Col, Form, Button, InputGroup } from 'react-bootstrap'
import Styles from './styles.module.css'
import { FaEye, FaEyeSlash } from 'react-icons/fa'
import { useState } from 'react'
import { GoogleLogin } from 'react-google-login';
import { InputText, InputPassword } from '../../components/formik/fields'
import { Formik, useField, useFormikContext } from "formik";
import * as Yup from "yup";
import router, { useRouter } from 'next/router';

export default function Home() {
    const[seePassword, setSeePassword] = useState(false)
    const { query } = useRouter();
    
    const responseGoogle = (response) => {
        console.log(response);
    }
    
  return (
    <div>
      <Head>
        <title>Central - Login</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <main>
          <section className={`${Styles.LoginContainer}`}>
              <Container>
                  <Row>
                      <Col sm={7} md={6} lg={5} className={`${Styles.formWrapper} offset-sm-5 offset-md-6 offset-lg-7`}>
                        <h1 className="font-weight-bold text-center color-primary">Hi, Welcome to</h1>
                        <div className="central-logo col-5 mx-auto my-4 text-center">
                            <img src="/images/central-logo.svg" className="img-fluid" alt="logo central"/>
                        </div>
                        <h5 className="login-to-account  color-primary text-center mb-4">Login to your account</h5>

                        <Formik
                            initialValues={{
                            email: "",
                            password: ""
                            }}
                            validationSchema={Yup.object({
                            email: Yup.string()
                                .email("Invalid email addresss")
                                .required("Required"),
                            password: Yup.string()
                                .min(8, "Must be 8 characters minimum")
                                .required("Required"),
                            })}
                            onSubmit={(values) => {
                                router.push(
                                  {
                                    pathname: '/cars',
                                    query: { ...values, page: 1 },
                                  },
                                  undefined,
                                  { shallow: true }
                                );
                              }}
                        >
                        {({ values, isValid, dirty }) => (
                            <Form>
                                <Form.Group controlId="formBasicEmail">
                                    <InputText label="Email" name="email" type="email" placeholder="your.email@mail.com" />
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <InputPassword label="Password" name="password" type={seePassword ? "text" : "password"}  placeholder="" seePassword={seePassword} setSeePassword={setSeePassword} />
                                </Form.Group>
                                <Form.Group controlId="formBasicCheckbox" className="d-flex justify-content-between">
                                    <Form.Check type="checkbox" label="Remember Me" className="small color-primary" />
                                    <div className="forget-password small color-primary">Forget password?</div>
                                </Form.Group>
                                <Button className="bg-color-primary text-white text-center btn-block py-2 rounded" type="submit" disabled={!(isValid && dirty)}>
                                    Login
                                </Button>
                            </Form>
                        )}
                        </Formik>
                        
                        <GoogleLogin
                            clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
                            buttonText="Or sign-in with google"
                            onSuccess={responseGoogle}
                            onFailure={responseGoogle}
                            // cookiePolicy={'single_host_origin'}
                            className="google-button btn-block justify-content-center mt-3"
                        />
                        <div className="sign-up-btn text-center mt-3 text-secondary">
                            Don't have an account? <span className="color-primary font-weight-bold">Sign up</span>
                        </div>
                      </Col>
                  </Row>
              </Container>
          </section>
      </main>

      <footer>
      </footer>
    </div>
  )
}
