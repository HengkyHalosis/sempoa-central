import React, { useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { FaCheckCircle, FaTimes } from 'react-icons/fa';
import CheckoutLayout from '../../components/layouts/checkoutLayout'
import styles from './styles.module.css'
import { InputText, InputTextArea, InputSelect } from '../../components/formik/fields'
import { Formik, useField, useFormikContext } from "formik";
import * as Yup from "yup";
import Multiselect from '../../components/formik/multiselect'

const Checkout = () => {
    const [addAddress, setAddAddress] = useState(false)

    const provinsi = [
        { value: 'DKI', label: 'DKI' },
        { value: 'Jawa Barat', label: 'Jawa Barat' },
        { value: 'Jawa Timur', label: 'Jawa Timur' },
        { value: 'Jawa Tengah', label: 'Jawa Tengah' },
        { value: 'Bali', label: 'Bali' },
    ]
    
    const kabupaten = [
        { value: 'Lebak', label: 'Lebak' },
        { value: 'Pandeglang', label: 'Pandeglang' },
        { value: 'Serang', label: 'Serang' },
        { value: 'Tangerang', label: 'Tangerang' },
        { value: 'Cilegon', label: 'Cilegon' },
    ]
    
    const kecamatan = [
        { value: 'Cengkareng', label: 'Cengkareng' },
        { value: 'Grogol', label: 'Grogol' },
        { value: 'Taman Sari', label: 'Taman Sari' },
        { value: 'Tambora', label: 'Tambora' },
        { value: 'Kebon Jeruk', label: 'Kebon Jeruk' },
    ]

    const products = [
        { value: 'Face Shield', label: 'Face Shield' },
        { value: 'Face Shield Kacamata', label: 'Face Shield Kacamata' },
        { value: 'Face Shield Anak', label: 'Face Shield Anak' },
        { value: 'Helm Full Face', label: 'Helm Full Face' },
    ];
    
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    return (
        <CheckoutLayout title="Checkout">
            <section className="py-4">
                <Container>
                    <Row>
                        <Col md={8}>
                            <div className="d-flex flex-column">
                                <div className="pesanan border-color-secondary p-3">
                                    <h6 className="color-primary px-3">Produk Pesanan</h6>
                                    <Container>
                                        <Row>
                                            <Col xs={3}>
                                                <div className={`${styles.productWrapper}`}>
                                                    <div className={`${styles.productImgWrapper} position-relative`}>
                                                        <div className={`${styles.productImgContent}`}>
                                                            <img src="/images/placeholder-image.png" className={`${styles.imgProduct}`} alt="placeholder" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs={9}>
                                                <Row className="">
                                                    <Col sm={8} className="brand">
                                                        <h6 className="font-weight-bold">MSI Pro 16 Multitouch All in One</h6>
                                                        <div className="text-secondary mb-2 small">036AU 15.6 Multitouch <span className="ml-2">#5769355</span></div>
                                                    </Col>
                                                    <Col sm={4} className="price text-sm-right">
                                                        <h6 className="price font-weight-bold">Rp. 8.369.000</h6>
                                                        <div className="quantity font-weight-bold">x2</div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Container>
                                </div>

                                <div className="alamat-kirim border-color-secondary p-3">
                                    <h6 className="color-primary px-3">Alamat Pengiriman</h6>
                                    <Container>
                                        <Row className="small">
                                            <Col xs={3}>
                                                <div>Indonesia Raya</div>
                                                <div>08119855889</div>
                                            </Col>
                                            <Col xs={7}>
                                                <div>(Alamat Kantor)</div>
                                                <div>Jalan Tampomas No. 8 Malabar</div>
                                                <div>Lengkong, Jawa Barat, Kota Bandung 40262 Indonesia</div>
                                            </Col>
                                            <Col xs={2} className="color-primary text-right">
                                                <strong>Ubah</strong>
                                            </Col>
                                        </Row>
                                    </Container>
                                    <Container className="py-3">
                                        <Row>
                                            <Col><div className="secondary-button text-center" onClick={() => setAddAddress(true)}>Tambah alamat baru</div></Col>
                                            <Col><div className="secondary-button text-center">Pilih alamat lain</div></Col>
                                        </Row>
                                    </Container>
                                </div>

                                <div className="jasa-pengiriman border-color-secondary p-3">
                                    <h6 className="color-primary px-3">Jasa Pengiriman</h6>
                                    <Container>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlSelect1">
                                                <Form.Control as="select">
                                                    {/* <option><img src="/images/jne.svg" alt="jne" /> JNE Regular</option> */}
                                                    <option>JNE Regular</option>
                                                    <option>JNE YES</option>
                                                    <option>TIKI Regular</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Form>
                                    </Container>
                                    <Container className="additional-info color-primary small">Additional Information</Container>
                                </div>

                                <div className="catatan border-color-secondary p-3">
                                    <h6 className="color-primary px-3">Catatan Pemesanan</h6>
                                    <Container>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlTextarea1">
                                                <Form.Control as="textarea" rows={4} placeholder="Tulis catatan" />
                                            </Form.Group>
                                        </Form>
                                    </Container>
                                    <Container className="additional-info color-primary small">Additional Information</Container>
                                </div>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="d-flex flex-column">
                                <div className="pesanan border-color-secondary p-3">
                                    <h6 className="color-primary px-3 pb-3 border-bottom">Detail Pembayaran</h6>
                                    <div className="total-harga d-flex justify-content-between small py-1">
                                        <div className="detail-text">Total harga produk</div>
                                        <div className="detail-price">Rp. 8.369.000</div>
                                    </div>
                                    <div className="ongkos d-flex justify-content-between small py-1">
                                        <div className="detail-text">Ongkos kirim (JNE Regular)</div>
                                        <div className="detail-price">Rp. 18.000</div>
                                    </div>
                                    <div className="potongan d-flex justify-content-between small  pt-1 pb-3 border-bottom">
                                        <div className="detail-text">Potongan ongkos kirim</div>
                                        <div className="detail-price">Rp. -</div>
                                    </div>
                                    <div className="total-pembayaran d-flex justify-content-between small py-2 border-bottom font-weight-bold">
                                        <div className="detail-text">Total Pembayaran</div>
                                        <div className="detail-price">Rp. 8.387.000</div>
                                    </div>
                                    <div className="payment-method-btn primary-button text-center mt-3">Pilih Metode Pembayaran</div>
                                </div>
                                <Container className="p-5 text-center d-none d-md-block">
                                    <img src="/images/cart-img.svg" alt="cart" className="img-fluid" />
                                </Container>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>

            {/* MODAL TAMBAH ALAMAT */}
            {addAddress ?
                <div className={`${styles.modalWrapper}`}>
                    <div className={`${styles.modalContainer}`}>
                        <FaTimes className={`${styles.closeModal}`} onClick={() => setAddAddress(false)} />
                        <h2 className={`${styles.modalTitle}`}>Tambah Alamat Baru</h2>
                        <div className={`${styles.modalContent}`}>
                            <Formik
                                initialValues={{
                                    alamatsebagai: "",
                                    penerima: "",
                                    handphone: "",
                                    telepon: "",
                                    provinsi: "",
                                    kabupaten: "",
                                    kecamatan: "",
                                    kodepos: "",
                                    alamatlengkap: "",
                                }}
                                validationSchema={Yup.object({
                                    alamatsebagai: Yup.string()
                                        .required("Required"),
                                    penerima: Yup.string()
                                        .min(8, "Must be 8 characters minimum")
                                        .max(50, "Must be 50 characters max")
                                        .required("Required"),
                                    handphone: Yup.string()
                                        .matches(phoneRegExp, 'Phone number is not valid')
                                        .required("Required"),
                                    telepon: Yup.string()
                                        .matches(phoneRegExp, 'Phone number is not valid'),
                                    provinsi: Yup.string()
                                        .required("Required"),
                                    kabupaten: Yup.string()
                                        .required("Required"),
                                    kecamatan: Yup.string()
                                        .required("Required"),
                                    alamatlengkap: Yup.string()
                                        .required("Required"),

                                })}
                                onSubmit={(values, { setSubmitting }) => {
                                    const payload = values
                                    
                                    setTimeout(() => {
                                        console.log(payload)
                                        setSubmitting(false);
                                    }, 1000);
                                }}
                            >
                                {({ values, isValid, dirty, touched, handleChange, handleSubmit, handleBlur, handleReset, setFieldValue, setFieldTouched, errors, validateForm, isSubmitting }) => (
                                    <Form onSubmit={handleSubmit}>
                                        <Form.Group controlId="formBasicAlamatSebagai">
                                            <InputText label="Alamat Sebagai" name="alamatsebagai" type="text" placeholder="Contoh: Rumah, Kantor, dll" />
                                        </Form.Group>
                                        
                                        <Form.Group controlId="formBasicPenerima">
                                            <InputText label="Nama Penerima" name="penerima" type="text" placeholder="Nama Penerima" />
                                        </Form.Group>

                                        <Form.Group controlId="formBasicHandphone">
                                            <InputText label="Nomor Handphone" name="handphone" type="text" placeholder="Nomor Handphone" />
                                        </Form.Group>

                                        <Form.Group controlId="formBasicPhone">
                                            <InputText label="Nomor Telepon" name="telepon" type="text" placeholder="Nomor Telepon" />
                                        </Form.Group>

                                        <Form.Group controlId="formBasicProvinsi">
                                            <InputSelect name="provinsi" label="Provinsi" placeholder="Pilih Provinsi">
                                                <option value="">Pilih Provinsi</option>
                                                {provinsi.map((prov) =>
                                                    <option key={prov.value} value={prov.value}>{prov.label}</option>
                                                )}
                                            </InputSelect>
                                        </Form.Group>
                                        
                                        <Form.Group controlId="formBasicKabupaten">
                                            <InputSelect name="kabupaten" label="Kabupaten" placeholder="Kabupaten / Kota">
                                                <option value="">Pilih Kabupaten / Kota</option>
                                                {kabupaten.map((kab) =>
                                                    <option key={kab.value} value={kab.value}>{kab.label}</option>
                                                )}
                                            </InputSelect>
                                        </Form.Group>
                                        
                                        <Form.Group controlId="formBasickecamatan">
                                            <InputSelect name="kecamatan" label="Kecamatan" placeholder="Pilih Kecamatan">
                                                <option value="">Pilih Kecamatan</option>
                                                {kecamatan.map((camat) =>
                                                    <option key={camat.value} value={camat.value}>{camat.label}</option>
                                                )}
                                            </InputSelect>
                                        </Form.Group>

                                        <Form.Group controlId="formBasicKodePos">
                                            <InputText label="Kode POS" name="kodepos" type="text" placeholder="Kode POS" />
                                        </Form.Group>

                                        <Form.Group controlId="formBasicAlamatLengkap">
                                            <InputTextArea name="alamatlengkap" rows={3} id="alamatlengkap" htmlFor="alamatlengkap" label="Alamat Lengkap" placeholder="Alamat Lengkap" />
                                        </Form.Group>

                                        <button className="btn-sm primary-button my-4 btn-block text-center d-block border-0" type="submit" disabled={!(isValid && dirty && !isSubmitting)}>
                                            Tambah Alamat
                                        </button>
                                        {/* <pre>
                                            {JSON.stringify(values, null, 2)}
                                        </pre> */}
                                    </Form>
                                )}
                            </Formik>
                        </div>
                    </div>
                </div>
                : null}
        </CheckoutLayout>
    );
};

export default Checkout;